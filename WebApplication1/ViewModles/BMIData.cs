﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApplication1.ViewModles
{
    public class BMIData
    {
        [Required(ErrorMessage ="輸入資料!!")]
        [Range(30,150,ErrorMessage="30-50")]
        [Display(Name ="體重")]
        public float Weight { get; set; }

        [Required(ErrorMessage ="輸入資料!!")]
        [Range(30, 150, ErrorMessage = "30-50")]
        [Display(Name = "身高")]
        public float Height { get; set; }
        public float BMI { get; set; }
        public string Level { get; set; }

    }
}